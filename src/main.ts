import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import * as dotenv from 'dotenv';
import { kafkaOptions } from './config/kafka.config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

dotenv.config();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const microservice = app.connectMicroservice({
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: process.env.KAFKA_CLIENT_ID || 'service-promo',
        brokers: [process.env.KAFKA_BROKERS || 'localhost:19092'],
      },
      consumer: {
        groupId: process.env.KAFKA_GROUPID || 'kezbek-consumer',
      },
    },
  });
  const config = new DocumentBuilder()
    .setTitle('KEZBEK Service-Promo')
    .setDescription('api document for service promo')
    .setVersion('1.0')
    .addTag('service-promo')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('document', app, document);

  await app.startAllMicroservices();
  await app.listen(3001);
}
bootstrap();
