import { DataSourceOptions } from 'typeorm';
import * as dotenv from 'dotenv';
import { Promo } from 'src/promo/entity/promo.entity';

dotenv.config();

export const dataSourceOptions: DataSourceOptions = {
  type: 'mysql',
  host: process.env.MYSQL_HOST || 'localhost',
  port: parseInt(process.env.MYSQL_PORT) || 3307,
  username: process.env.MYSQL_USER || 'admin',
  password: process.env.MYSQL_PASSWD || 'password',
  database: process.env.MYSQL_DB || 'service_promo',
  entities: [Promo],
  synchronize: true,
};
