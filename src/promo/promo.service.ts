import { Injectable } from '@nestjs/common';
import { PromoRepository } from './repository/promo.repository';
import { Promo } from './entity/promo.entity';
import { CashbackPromo } from './cashback-promo';
import { KezbekMessageDto } from './dto/kezbek-message.dto';
import { LoyaltyMessageDto } from './dto/loyalty_message.dto';

@Injectable()
export class PromoService {
  cashbackPromo: CashbackPromo;
  constructor(private readonly promoRepository: PromoRepository) {
    this.cashbackPromo = new CashbackPromo();
  }

  async getPromoByCode(promoCode: string): Promise<Promo[]> {
    const result = await this.promoRepository.getPromoByCode(promoCode);

    return result;
  }

  async calculatePromo(
    kezbekMessageDto: KezbekMessageDto,
  ): Promise<LoyaltyMessageDto> {
    const promoCode = kezbekMessageDto['promo_code'];
    const result = await this.getPromoByCode(promoCode);
    const cashback = await this.cashbackPromo.getCashback(
      result[0],
      kezbekMessageDto,
    );
    const cashbackPromo = { cashback_promo: cashback };
    const messageLoyalty = { ...kezbekMessageDto, ...cashbackPromo };

    return messageLoyalty;
  }
}
