import { Test, TestingModule } from '@nestjs/testing';
import { PromoService } from './promo.service';
import { PromoRepository } from './repository/promo.repository';
import { KezbekMessageDto } from './dto/kezbek-message.dto';

describe('PromoService', () => {
  let promoService: PromoService;

  beforeEach(async () => {
    const fakeRepo: Partial<PromoRepository> = {
      getPromoByCode: (promo) =>
        Promise.resolve([
          {
            id: 1,
            promo_name: 'promo akhir tahun',
            promo_code: 'DEC001',
            promo_rule:
              '{"promo_1":{"min_product":1,"rule":[{"checkout_value_min":0,"checkout_value_max":100000,"percent_cashback":0.012},{"checkout_value_min":100000,"checkout_value_max":500000,"percent_cashback":0.0175},{"checkout_value_min":500000,"checkout_value_max":1000000,"percent_cashback":0.023}]},"promo_2":{"min_product":2,"rule":[{"checkout_value_min":500000,"checkout_value_max":1000000,"percent_cashback":0.0245},{"checkout_value_min":1000000,"checkout_value_max":1500000,"percent_cashback":0.0275},{"checkout_value_min":1500000,"checkout_value_max":10000000000,"percent_cashback":0.0295}]},"promo_3":{"min_product":3,"rule":[{"checkout_value_min":1500000,"checkout_value_max":15000000000,"percent_cashback":0.0335}]}}',
          },
        ]),
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PromoService,
        {
          provide: PromoRepository,
          useValue: fakeRepo,
        },
      ],
    }).compile();

    promoService = module.get<PromoService>(PromoService);
  });

  it('should be defined', () => {
    expect(promoService).toBeDefined();
  });

  it('should return cashback 1.2%', async () => {
    const input = new KezbekMessageDto();
    input.purchase_quantity = 1;
    input.purchase_amount = 90000;
    const expectedResult = 0.012 * input.purchase_amount;

    const output = await promoService.calculatePromo(input);
    expect(output['cashback_promo']).toBe(expectedResult);
  });

  it('should return cashback 1.75%', async () => {
    const input = new KezbekMessageDto();
    input.purchase_quantity = 1;
    input.purchase_amount = 400000;
    const expectedResult = 0.0175 * input.purchase_amount;

    const output = await promoService.calculatePromo(input);
    expect(output['cashback_promo']).toBe(expectedResult);
  });

  it('should return cashback 2.3%', async () => {
    const input = new KezbekMessageDto();
    input.purchase_quantity = 1;
    input.purchase_amount = 900000;
    const expectedResult = 0.023 * input.purchase_amount;

    const output = await promoService.calculatePromo(input);
    expect(output['cashback_promo']).toBe(expectedResult);
  });

  it('should return cashback 2.45%', async () => {
    const input = new KezbekMessageDto();
    input.purchase_quantity = 2;
    input.purchase_amount = 600000;
    const expectedResult = 0.0245 * input.purchase_amount;

    const output = await promoService.calculatePromo(input);
    expect(output['cashback_promo']).toBe(expectedResult);
  });

  it('should return cashback 2.75%', async () => {
    const input = new KezbekMessageDto();
    input.purchase_quantity = 2;
    input.purchase_amount = 1200000;
    const expectedResult = 0.0275 * input.purchase_amount;

    const output = await promoService.calculatePromo(input);
    expect(output['cashback_promo']).toBe(expectedResult);
  });

  it('should return cashback 2.95%', async () => {
    const input = new KezbekMessageDto();
    input.purchase_quantity = 2;
    input.purchase_amount = 1500000;
    const expectedResult = 0.0295 * input.purchase_amount;

    const output = await promoService.calculatePromo(input);
    expect(output['cashback_promo']).toBe(expectedResult);
  });

  it('should return cashback 3.35%', async () => {
    const input = new KezbekMessageDto();
    input.purchase_quantity = 3;
    input.purchase_amount = 12000000;
    const expectedResult = 0.0335 * input.purchase_amount;

    const output = await promoService.calculatePromo(input);
    expect(output['cashback_promo']).toBe(expectedResult);
  });


});
