import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('promo_instant')
export class Promo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  promo_name: string;

  @Column()
  promo_code: string;

  @Column({ type: 'mediumtext' })
  promo_rule: string;
}
