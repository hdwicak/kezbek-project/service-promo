import { KezbekMessageDto } from './dto/kezbek-message.dto';
import { Promo } from './entity/promo.entity';

export class CashbackPromo {
  async getCashback(promo: Promo, kezbekMessageDto: KezbekMessageDto) {
    const obj = promo['promo_rule'].replace('\\', '');
    const jsonRules = JSON.parse(obj);
    const outputRules = Object.values(jsonRules);

    const listPromoRules = [];
    const quantityRule = [];
    let instantCashback = 0;

    outputRules.forEach((item) => {
      quantityRule.push(item['min_product']);
    });
    const maxQuantityRule = Math.max(...quantityRule);

    if (kezbekMessageDto['purchase_quantity'] >= maxQuantityRule) {
      const promoMax = outputRules.filter(
        (item) => item['min_product'] === maxQuantityRule,
      );

      promoMax.forEach((item) => {
        item['rule'].forEach((data) => {
          listPromoRules.push(data);
        });
      });
    } else {
      outputRules.forEach((item) => {
        if (item['min_product'] === kezbekMessageDto['purchase_quantity']) {
          item['rule'].forEach((data) => {
            listPromoRules.push(data);
          });
        }
      });
    }

    const tempMinPurchaseArray = [];
    listPromoRules.forEach((item) => {
      tempMinPurchaseArray.push(item['checkout_value_max']);
    });

    const minPurchaseRules = Math.min(...tempMinPurchaseArray);

    if (kezbekMessageDto['purchase_amount'] < minPurchaseRules) {
      const filteredRule = listPromoRules.filter(
        (item) => item['checkout_value_max'] === minPurchaseRules,
      );

      if (
        filteredRule[0]['checkout_value_min'] <=
          kezbekMessageDto['purchase_amount'] &&
        kezbekMessageDto['purchase_amount'] <
          filteredRule[0]['checkout_value_max']
      ) {
        instantCashback = filteredRule[0]['percent_cashback'];
      }
    } else {
      let updatedListPromoRules;

      if (listPromoRules.length !== 1) {
        updatedListPromoRules = listPromoRules.splice(1);
      } else {
        updatedListPromoRules = listPromoRules;
      }
      updatedListPromoRules.forEach((item) => {
        if (
          item['checkout_value_min'] <= kezbekMessageDto['purchase_amount'] &&
          kezbekMessageDto['purchase_amount'] < item['checkout_value_max']
        ) {
          instantCashback = item['percent_cashback'];
        }
      });
    }

    const promoCashbackAmount =
      kezbekMessageDto.purchase_amount * instantCashback;

    return promoCashbackAmount;
  }
}
