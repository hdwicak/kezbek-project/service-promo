import { Injectable } from '@nestjs/common';
import { Promo } from '../entity/promo.entity';
import { DataSource, Repository } from 'typeorm';
import { CreatePromoDto } from '../dto/create-promo.dto';

@Injectable()
export class PromoRepository extends Repository<Promo> {
  constructor(private readonly dataSource: DataSource) {
    super(Promo, dataSource.createEntityManager());
  }

  async createPromo(promoDto: CreatePromoDto): Promise<Promo> {
    return await this.save(promoDto);
  }

  async getPromoByCode(promoCode: string): Promise<Promo[]> {
    return await this.find({
      where: { promo_code: promoCode },
    });
  }
}
