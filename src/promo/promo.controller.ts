import { Controller, Get, Inject, Logger, Param } from '@nestjs/common';
import { PromoService } from './promo.service';
import { Promo } from './entity/promo.entity';
import { ClientKafka, EventPattern, Payload } from '@nestjs/microservices';
import { KezbekMessageDto } from './dto/kezbek-message.dto';
import * as dotenv from 'dotenv';
import { ApiTags } from '@nestjs/swagger';

dotenv.config();
@ApiTags('service-promo')
@Controller('promo')
export class PromoController {
  constructor(
    private readonly promoService: PromoService,

    @Inject(process.env.KAFKA_TOKEN || 'KAFKA_SERVICE_PROMO')
    private readonly client: ClientKafka,

    private readonly logger: Logger,
  ) {}

  @Get(':code')
  async getPromoCode(@Param('code') code: string) {
    const result = await this.promoService.getPromoByCode(code);

    return result;
  }

  @EventPattern('add-transactions')
  async calculateCashback(@Payload() data: KezbekMessageDto) {
    try {
      this.logger.log(
        '[KEZBEK-SERVICE-PROMO] processing incoming message topic : add-transactions',
      );
      const promoCode = data.promo_code;
      const validPromo = this.getPromoCode(promoCode);
      if (validPromo) {
        const messageLoyalty = await this.promoService.calculatePromo(data);

        await this.client.emit(
          'incoming-service-loyalty',
          JSON.stringify(messageLoyalty),
        );
      } else {
        this.logger.log(
          '[KEZBEK-SERVICE-PROMO] error promo code not eligible, emit message to topic : error-transaction',
        );
        await this.client.emit('error-transaction', JSON.stringify(data));
      }
    } catch (err) {
      this.logger.error(err);
    }
  }
}
