import { CashbackPromo } from './cashback-promo';
import { KezbekMessageDto } from './dto/kezbek-message.dto';

describe('instant cashback promo', () => {
  let input: KezbekMessageDto;

  const ruleFromDb = 
  {
    "id": 1,
    "promo_name": "promo akhir tahun",
    "promo_code": "DEC001",
    "promo_rule": "{\"promo_1\":{\"min_product\":1,\"rule\":[{\"checkout_value_min\":0,\"checkout_value_max\":100000,\"percent_cashback\":0.012},{\"checkout_value_min\":100000,\"checkout_value_max\":500000,\"percent_cashback\":0.0175},{\"checkout_value_min\":500000,\"checkout_value_max\":1000000,\"percent_cashback\":0.023}]},\"promo_2\":{\"min_product\":2,\"rule\":[{\"checkout_value_min\":500000,\"checkout_value_max\":1000000,\"percent_cashback\":0.0245},{\"checkout_value_min\":1000000,\"checkout_value_max\":1500000,\"percent_cashback\":0.0275},{\"checkout_value_min\":1500000,\"checkout_value_max\":1500000,\"percent_cashback\":0.0295}]},\"promo_3\":{\"min_product\":3,\"rule\":[{\"checkout_value_min\":1500000,\"checkout_value_max\":15000000000,\"percent_cashback\":0.0335}]}}"
}
  const cashbackPromo = new CashbackPromo();

  it('should return cashback ', async () => {
    input = new KezbekMessageDto();
    input.purchase_amount = 500000;
    input.purchase_quantity = 2;
    const result = await cashbackPromo.getCashback(ruleFromDb, input);
    const expectedResult = 0.0245 * input.purchase_amount;
    expect(result).toBe(expectedResult);
  });
});
