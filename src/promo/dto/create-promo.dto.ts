import { IsNotEmpty } from 'class-validator';

export class CreatePromoDto {
  @IsNotEmpty()
  promo_name: string;

  @IsNotEmpty()
  promo_code: string;

  @IsNotEmpty()
  promo_rule: string;
}
