import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class KezbekMessageDto {
  // @IsString()
  // @IsNotEmpty()
  trx_date: string;

  // @IsString()
  // @IsNotEmpty()
  partner_code: string;

  // @IsString()
  // @IsNotEmpty()
  partner_key: string;

  // @IsString()
  // @IsNotEmpty()
  promo_code: string;

  // @IsString()
  // @IsNotEmpty()
  customer_email: string;

  // @IsString()
  // @IsNotEmpty()
  payment_wallet: string;

  // @IsString()
  // @IsNotEmpty()
  order_id: string;

  // @IsNumber()
  // @IsNotEmpty()
  purchase_quantity: number;

  // @IsNumber()
  // @IsNotEmpty()
  purchase_amount: number;

  // @IsString()
  // @IsNotEmpty()
  description: string;
}
