import { Logger, Module } from '@nestjs/common';
import { PromoController } from './promo.controller';
import { PromoService } from './promo.service';
import { PromoRepository } from './repository/promo.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Promo } from './entity/promo.entity';
import { CashbackPromo } from './cashback-promo';
import { ClientsModule } from '@nestjs/microservices';
import { kafkaOptions } from 'src/config/kafka.config';

@Module({
  imports: [
    TypeOrmModule.forFeature([Promo]),
    ClientsModule.register(kafkaOptions),
  ],
  controllers: [PromoController],
  providers: [PromoService, PromoRepository, CashbackPromo, Logger],
})
export class PromoModule {}
