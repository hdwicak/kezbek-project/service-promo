import { Module } from '@nestjs/common';
import { ClientsModule } from '@nestjs/microservices';
import { kafkaOptions } from './config/kafka.config';
import { PromoModule } from './promo/promo.module';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { dataSourceOptions } from './config/data-source.config';

@Module({
  imports: [
    ClientsModule.register(kafkaOptions),
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot(dataSourceOptions),
    PromoModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
